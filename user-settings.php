<?php include 'header.php'; ?>
<?php include 'nav.php'; ?>
<body>
<div class="notification-bar">
	<div class="invite-notice">
		<h4>New Group Invites <span class="invite-count">4</span></h4>
	</div>
	<div class="group-joined-notice">
		<h4>Group Joined!</h4>
	</div>
</div><!--/notification-bar-->
<div class="invite-modal">
</div>

<div class="page user-settings-page">
	<div class="row">
		<div class="user-settings column large-8 medium-8 small-12">
			<div class="user-profile">
				<div class="profile-img"><img src="assets/img/users/yoona.png"></div>
				<h2 class="username">Yoona</h2>
				<div class="user-about">
"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.”
<br><span>Edit</span>
				</div>
			</div>
			<div class="settings-fields">
				<h2>Settings</h2>	
				<label>Change Email Address</label>
				<input type="text" placeholder="Change Email Address">
				<label>Change Password</label>
				<input type="text" placeholder="Change Password">
				<input type="text" placeholder="Confirm Password">
				<button class="form-btn">Save Changes</button>
				<a class="terminate-account" href="#">Terminate Account</a>
			</div>
		</div>
	</div>
</div>
</body>
<html>
