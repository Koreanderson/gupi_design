<?php include 'header.php'; ?>
<?php include 'nav.php'; ?>

<?php //include 'notification-bar.php'; ?>
<body>
<!-- 
When the invite overlay and modal appear, we need to make sure that we add 'position: fixed;' to the .page div' to make sure it doesn't scroll when the modal is active 
-->
<?php include 'post_modals.php'; ?>

<div class="page single-group">
	<div class="row">
		<div class="create-post-block column large-8 medium-8 small-12">
			<div class="create-post-header">
				<div class="post-author">
					<img src="/assets/img/users/yoona.png">
				</div>
			</div>
			<div class="post-types">
				<button class="create-text-post">
					<i class="fa fa-commenting"></i>
					<span>Text</span>
				</button>
				<button class="create-text-post">
					<i class="fa fa-link"></i>
					<span>Link</span>
				</button>
				<button class="create-img-post">
					<i class="fa fa-camera-retro"></i>
					<span>Image</span>
				<button class="create-vid-post">
					<i class="fa fa-video-camera"></i>
					<span>Video</span>
				</button>
				</button>
			</div>
		</div>
		<div class="post-feed column large-8 medium-8 small-12">
			<div class="post">
				<div class="post-author">
					<img src="/assets/img/users/jessica.png">
					<span>Jessica</span>
				</div>
				<div class="post-date">
					Sep 16
				</div>
				<div class="post-content text-post">
					<h3>Some Heading</h3>
					WoooOOOoooooO00000rrrd
				</div>
			</div>
			<div class="post">
				<div class="post-author">
					<img src="/assets/img/users/iu.png">
					<span>IU</span>
				</div>
				<div class="post-date">
					Sep 16
				</div>
				<div class="post-content text-post">
Lorem ipsum dolor sit amet, atqui dicunt mea at, mei no vitae dictas iudicabit. At per autem nominavi, mea alienum tacimates ei, iudicabit expetendis reformidans pri ei. Mel te paulo feugait, iriure omnesque maluisset et eos. Copiosae mediocrem deterruisset mei at. Ad saepe nostro nam. Ne est debet necessitatibus, soluta menandri omittantur nam cu.
Lorem ipsum dolor sit amet, atqui dicunt mea at, mei no vitae dictas iudicabit. At per autem nominavi, mea alienum tacimates ei, iudicabit expetendis reformidans pri ei. Mel te paulo feugait, iriure omnesque maluisset et eos. Copiosae mediocrem deterruisset mei at. Ad saepe nostro nam. Ne est debet necessitatibus, soluta menandri omittantur nam cu.
Lorem ipsum dolor sit amet, atqui dicunt mea at, mei no vitae dictas iudicabit. At per autem nominavi, mea alienum tacimates ei, iudicabit expetendis reformidans pri ei. Mel te paulo feugait, iriure omnesque maluisset et eos. Copiosae mediocrem deterruisset mei at. Ad saepe nostro nam. Ne est debet necessitatibus, soluta menandri omittantur nam cu.
					
				</div>
				<div class="post-options">
					<i class="fa fa-ellipsis-h"></i>
				</div>
			</div>
			<div class="post">
				<div class="post-author">
					<img src="/assets/img/users/yoona.png">
					<span>Yoona</span>
				</div>
				<div class="post-date">
					Sep 16
				</div>
				<div class="post-content image-post">
					<img src="assets/img/yoona_beach.png"/>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
<html>

