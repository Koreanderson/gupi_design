<?php include 'header.php'; ?>
<?php include 'nav.php'; ?>
<body>
<?php include 'notification-bar.php'; ?>
<div class="invite-modal">
</div>
	<div class="page group-list-page">
		<div class="row">
			<div class="create-post-block column large-8 medium-8 small-12">
				<div class="create-post-header">
					<div class="post-author">
						<img src="/assets/img/users/yoona.png">
					</div>
				</div>
				<div class="post-types">
					<button class="create-text-post">
						<i class="fa fa-commenting"></i>
						<span>Create</span>
					</button>
					<button class="create-text-post">
						<i class="fa fa-link"></i>
						<span>Account</span>
					</button>
					<button class="create-img-post">
						<i class="fa fa-camera-retro"></i>
						<span>Logout</span>
					</button>
				</div>
			</div>
			<div class="group-list column large-8 medium-8 small-12">
				<div class="group">
					<a href="group-feed.php"><h2>Group Name</h2>
					<div class="group-banner">
						<img src="assets/img/group_banner.png"/>
					</div>
					</a>
					<div class="group-members">
						<div class="user">
							<img src="assets/img/users/jessica.png"/>
						</div>
						<div class="user">
							<img src="assets/img/users/iu.png"/>
						</div>
						<div class="user">
							<img src="assets/img/users/yoona.png"/>
						</div>
					</div>
				</div><!--/group-->
				<div class="group">
					<h2>Group Name</h2>
					<div class="group-banner">
						<img src="assets/img/group_banner.png"/>
					</div>
					<div class="group-members">
						<div class="user">
							<img src="assets/img/users/jessica.png"/>
						</div>
						<div class="user">
							<img src="assets/img/users/iu.png"/>
						</div>
						<div class="user">
							<img src="assets/img/users/yoona.png"/>
						</div>
						<div class="more-users">
							+ <span class="more-user-count">4</span> Others
						</div>
					</div>
				</div><!--/group-->
			</div>
		</div>
	</div>
</body>
<html>
