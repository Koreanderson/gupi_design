jQuery(document).ready(function($){

	//Login Form

	$(".register-form").hide();
	$(".login-form").find(".create-account").click(function(){
		event.preventDefault();
		$(".login-form").fadeOut(400),
		$(".register-form").delay(300).fadeIn(400);
	});
	$(".login-form").find(".login").click(function(){
		event.preventDefault();
		window.location='groups.php';
	});

	//Invite Modal
	
	$(".invite-overlay").hide();
	$(".invite-notice").click(function(){
		$(".invite-overlay").fadeIn(200);
	});
	$(".invite-overlay-bg").click(function(){
		$(".invite-overlay").fadeOut(50);
	});
	
	//Image Post Resize

	$(".image-post").click(function(){
		$(this).toggleClass("open");
	});

	$(".text-post").click(function(){
		$(this).toggleClass("open");
	});

});
