<div class="invite-overlay">
	<div class="invite-overlay-bg"></div>
	<div class="invite-modal column large-5">
		<h2>Group Invites</h2>

		<div class="group-invite">
			<div class="user"><img src="assets/img/users/yoona.png"/></div>
			<h4><span class="sender">Yoona</span> invited you to the group <span clas="group-invite-name">Neat Group</span></h4>
			<div class="group-banner">
				<img src="assets/img/group_banner.png"/>
			</div>
			<div class="invite-response">
				<button class="decline" type="button"><i class="fa fa-times"></i></button>
				<button class="accept" type="button"><i class="fa fa-check"></i></button>
			</div>
		</div>

		<div class="group-invite">
			<div class="user"><img src="assets/img/users/iu.png"/></div>
			<h4><span class="sender">IU</span> invited you to the group <span clas="group-invite-name">Awesome Group</span></h4>
			<div class="group-banner">
				<img src="assets/img/group_banner.png"/>
			</div>
			<div class="invite-response">
				<button class="decline" type="button"><i class="fa fa-times"></i></button>
				<button class="accept" type="button"><i class="fa fa-check"></i></button>
			</div>
		</div>

	</div>
</div>
